# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/ghotrix/MyCompany/MyGame/cocos2d/external/tinyxml2/tinyxml2.cpp" "/home/ghotrix/MyCompany/MyGame/cocos2d/external/tinyxml2/CMakeFiles/tinyxml2.dir/tinyxml2.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "CC_ENABLE_BOX2D_INTEGRATION=0"
  "CC_ENABLE_CHIPMUNK_INTEGRATION=1"
  "LINUX"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "cocos2d/cocos"
  "cocos2d"
  "cocos2d/deprecated"
  "cocos2d/cocos/platform"
  "cocos2d/extensions"
  "cocos2d/external"
  "/usr/include/GLFW"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
