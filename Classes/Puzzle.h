#ifndef _PUZZLE_H_
#define _PUZZLE_H_

#define BOARD_SIDE 4
#define BOARD_SIZE BOARD_SIDE*BOARD_SIDE

class Puzzle {

public:
    Puzzle();
    ~Puzzle() {}

    bool isSolved();
    void shuffle();
    int swap(int i);
    int getRockNumber(int i);

private:
    bool isSolvable();
    bool isBlank(int i);
    void swap(int i, int j);

    int _board[BOARD_SIZE];
};

#endif
