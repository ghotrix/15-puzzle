#include "Puzzle.h"
#include <random>
#include <iostream>
#include <chrono>

using namespace std;

Puzzle::Puzzle()
{
    for (int i = 0; i < BOARD_SIZE; i++) {
        _board[i] = i + 1;
    }
    shuffle();
}

/*
 * Every next element must be greater then previous.
*/
bool Puzzle::isSolved()
{
    for (int i = 1; i < BOARD_SIZE; i++) {
        if (_board[i] < _board[i-1])
            return false;
    }

    return true;
}

/*
    Knuth fast shuffling.
*/
void Puzzle::shuffle()
{
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    default_random_engine engine(seed);
    /*
     * Once in 15! times it can be solved from a very beginning,
     * so we're checking for it. Also half of combinations isn't solvable
     * at all.
    */
    do {
        for (int i = 0; i < BOARD_SIZE; i++) {
            uniform_int_distribution<int> uniform(0,i);
            int r = uniform(engine);
            swap(i, r);
        }
    }
    while(isSolved() || !isSolvable());
}

int Puzzle::swap(int i)
{
    if (i < 0)
        return -1;

    /* Right swap.*/
    if (i % BOARD_SIDE != BOARD_SIDE - 1 && isBlank(i + 1)) {
        swap(i, i + 1);
        return i + 1;
    }

    /* Left swap. */
    if (i % BOARD_SIDE != 0 && isBlank(i - 1)) {
        swap(i, i - 1);
        return i - 1;
    }

    /* Upper swap. */
    if (i >= BOARD_SIDE && isBlank(i - BOARD_SIDE)) {
        swap(i, i - BOARD_SIDE);
        return i - BOARD_SIDE;
    }

    /* Lower swap. */
    if (i < BOARD_SIZE - BOARD_SIDE && isBlank(i + BOARD_SIDE)) {
        swap(i, i + BOARD_SIDE);
        return i + BOARD_SIDE;
    }

    return -1;
}

int Puzzle::getRockNumber(int i)
{
    return _board[i];
}

void Puzzle::swap(int i, int j)
{
    int tmp = _board[i];
    _board[i] = _board[j];
    _board[j] = tmp;
}

/*
 * Half of the positions aren't solvable, so we need to check that
 * to not disappoint user :)
 * Puzzle is solvable if:
 * - board width is odd and number of inversions is even;
 * - board width is even and the blank is on even row, then number of inversions must be odd;
 * - board width is even and the blank is on odd row, then number of inversions must be even;
*/
bool Puzzle::isSolvable()
{
    int N = 0;
    int lineWithBlank = BOARD_SIDE - 1;

    /* Counting inversions.
     * No need to check for 1 (there are no lesser items)
     * and for blank (which is equal to BOARD_SIZE).
    */
    for (int i = 0; i < BOARD_SIZE; i++) {
        if (_board[i] == BOARD_SIZE) {
            lineWithBlank = i / BOARD_SIDE;
            continue;
        }
        if (_board[i] == 1)
            continue;

        for (int j = i + 1; j < BOARD_SIZE; j++) {
            if (_board[i] > _board[j]) N++;
        }
    }

    return (BOARD_SIDE % 2 == 0 && ((lineWithBlank % 2 == 1) == (N % 2 == 0))) || (BOARD_SIDE % 2 == 1 && N % 2 == 0);
}

bool Puzzle::isBlank(int i)
{
    return _board[i] == BOARD_SIZE;
}
