#ifndef __MAIN_SCENE_H__
#define __MAIN_SCENE_H__

#include <cocos2d.h>

class Puzzle;

class MainScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene *createScene();

    virtual bool init();
    void menuCloseCallback(cocos2d::Ref *pSender);

    CREATE_FUNC(MainScene);

private:
    void drawRocks();
    void createEventDispatchers();

    Puzzle *_puzzle;
    std::vector<cocos2d::Sprite *> _rocks;
    int _rockSize;
    cocos2d::Size _visibleSize;
    cocos2d::Vec2 _origin;
};

#endif
