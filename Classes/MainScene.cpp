#include "MainScene.h"
#include "Puzzle.h"
#include <cocos2d.h>
#include <iostream>

USING_NS_CC;

Scene* MainScene::createScene()
{
    // return the scene
    auto scene = Scene::create();

    auto layer = MainScene::create();
    scene->addChild(layer);

    return scene;
}

// on "init" you need to initialize your instance
bool MainScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    _puzzle = new Puzzle();

    _visibleSize = Director::getInstance()->getVisibleSize();
    _origin = Director::getInstance()->getVisibleOrigin();
    _rockSize = 96;
    drawRocks();
    createEventDispatchers();


    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(MainScene::menuCloseCallback, this));
    
    closeItem->setPosition(Vec2(_origin.x + _visibleSize.width - closeItem->getContentSize().width/2 ,
                                _origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
    
    return true;
}


void MainScene::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void MainScene::drawRocks()
{
    Sprite *rock = NULL;
    Label *label = NULL;

    if (_rocks.size() != BOARD_SIZE - 1) {
        for (int i = 0; i < BOARD_SIZE - 1; i++) {
            Vec2 position = Vec2(_visibleSize.width/2-_rockSize*BOARD_SIDE/2+i%BOARD_SIDE*_rockSize+_rockSize/2,
                                 _visibleSize.height/2+_rockSize*BOARD_SIDE/2-_rockSize*(i/BOARD_SIDE)-_rockSize/2);

            rock = Sprite::create("rect.png");
            _rocks.push_back(rock);
            float rockSize = rock->getContentSize().width;
            rock->setPosition(position);
            rock->setScale(_rockSize/rockSize);
            rock->setTag(i);
            label = Label::createWithTTF(std::to_string(i + 1), "fonts/Marker Felt.ttf", 48);
            label->setPosition(rockSize/2, rockSize/2);

            this->addChild(rock, 2);
            rock->addChild(label, 0);
        }
    }

    for (int i = 0; i < BOARD_SIZE; i++) {
        int rockNumber = _puzzle->getRockNumber(i);
        if (rockNumber == BOARD_SIZE)
            continue;

        Vec2 position = Vec2(_visibleSize.width/2-_rockSize*BOARD_SIDE/2+i%BOARD_SIDE*_rockSize+_rockSize/2,
                             _visibleSize.height/2+_rockSize*BOARD_SIDE/2-_rockSize*(i/BOARD_SIDE)-_rockSize/2);

        rock = _rocks.at(rockNumber - 1);
        rock->setPosition(position);
        rock->setTag(i);
    }

}

void MainScene::createEventDispatchers()
{
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchEnded = [=](Touch *touch, Event *event) {
        Node *parentNode = event->getCurrentTarget();
        Vector<Node *> children = parentNode->getChildren();
        Point touchPosition = parentNode->convertTouchToNodeSpace(touch);
        for (auto iter = children.rbegin(); iter != children.rend(); ++iter) {
            Node *childNode = *iter;
            if (childNode->getBoundingBox().containsPoint(touchPosition)) {
                int swapped = _puzzle->swap(childNode->getTag());
                if (swapped >= 0) {
                    drawRocks();
                }
                if (_puzzle->isSolved()) {
                    _puzzle->shuffle();
                    drawRocks();
                }
                return true;
            }
        }
        return false;
    };

    listener->onTouchBegan = [&](Touch *touch, Event *event) {
        return true;
    };


    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}
