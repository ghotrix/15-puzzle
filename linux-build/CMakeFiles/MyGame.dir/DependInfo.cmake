# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/ghotrix/MyCompany/MyGame/Classes/AppDelegate.cpp" "/home/ghotrix/MyCompany/MyGame/linux-build/CMakeFiles/MyGame.dir/Classes/AppDelegate.cpp.o"
  "/home/ghotrix/MyCompany/MyGame/Classes/MainScene.cpp" "/home/ghotrix/MyCompany/MyGame/linux-build/CMakeFiles/MyGame.dir/Classes/MainScene.cpp.o"
  "/home/ghotrix/MyCompany/MyGame/Classes/Puzzle.cpp" "/home/ghotrix/MyCompany/MyGame/linux-build/CMakeFiles/MyGame.dir/Classes/Puzzle.cpp.o"
  "/home/ghotrix/MyCompany/MyGame/proj.linux/main.cpp" "/home/ghotrix/MyCompany/MyGame/linux-build/CMakeFiles/MyGame.dir/proj.linux/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "LINUX"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/ghotrix/MyCompany/MyGame/linux-build/cocos2d/cocos/CMakeFiles/cocos2d.dir/DependInfo.cmake"
  "/home/ghotrix/MyCompany/MyGame/linux-build/cocos2d/external/unzip/CMakeFiles/unzip.dir/DependInfo.cmake"
  "/home/ghotrix/MyCompany/MyGame/linux-build/cocos2d/external/tinyxml2/CMakeFiles/tinyxml2.dir/DependInfo.cmake"
  "/home/ghotrix/MyCompany/MyGame/linux-build/cocos2d/external/xxhash/CMakeFiles/xxhash.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include/GLFW"
  "/usr/include/GLFW"
  "../cocos2d/cocos"
  "../Classes"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
